#!/usr/bin/env perl
use strict;
use warnings;
use Daemon::Control;

use constant PROGRAM_DIR   => '/opt/perls4lg/';
use constant RUN_DIR   => '/var/run/';
use constant LOG_DIR   => '/var/log/';

use constant LOG_FILE         => LOG_DIR . 'autodial.log';
use constant PID_FILE         => RUN_DIR . 'autodiald.pid';

use constant DIAL_SCRIPT      => PROGRAM_DIR . 'dial.pl';

use constant SERVER_HOST      => '127.0.0.1';
use constant SERVER_PORT      => 7777;

use constant PHONE_NUMBER     => '+7343123456';
use constant SESSION_DURATION => 180;
use constant SPK_LEVEL        => 160;
use constant MIC_LEVEL        => 30;
use constant INPUT_NUM        => 1;

exit Daemon::Control->new(
	name         => 'autodiald',
	lsb_sdesc    => 'Autostart voice session daemon for Lineguard trackers',
	lsb_desc     => 'Decodes messages and autosends start voice session command for Lineguard trackers
',
	path         => PROGRAM_DIR . 'autodiald.pl',
	
	program      => PROGRAM_DIR . 'autodial.pl',
	program_args => ['--logfile', LOG_FILE, '--host', SERVER_HOST, '--port', SERVER_PORT, '--phone', PHONE_NUMBER, '--duration', SESSION_DURATION, '--slevel', SPK_LEVEL, '--mlevel', MIC_LEVEL, '--input', INPUT_NUM, '--dialscript', DIAL_SCRIPT],
	
	pid_file     => PID_FILE,

	stdout_file  => '/dev/null',
	stderr_file  => '/dev/null'
)->run;
