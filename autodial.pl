#!/usr/bin/env perl
use strict;
use warnings;
use Socket;
use Getopt::Long;
use Log::Any qw($log);
use Log::Any::Adapter;
use Log::Dispatch;
use Log::Dispatch::Screen;
use Log::Dispatch::File;
use Pod::Usage;
use POSIX qw(strftime);

my $VERSION='1.0';

my($logfile, $server_port, $server_host, $phone_number, $session_duration, $spk_level, $mic_level, $input_num, $man, $help, $version, $dialscript);

$server_port = 7777;
$server_host = 'localhost';

GetOptions('logfile=s'  => \$logfile,
           'port=i'     => \$server_port,
           'host=s'     => \$server_host,
           'phone=s'    => \$phone_number,
           'duration=i' => \$session_duration,
           'slevel=i'   => \$spk_level,
           'mlevel=i'   => \$mic_level,
           'input=i'    => \$input_num,
           'help'       => \$help,
           'man'        => \$man,
           'version'    => \$version,
	   'dialscript=s' => \$dialscript) or die('Error in command line arguments');

pod2usage(1) if $help;
pod2usage(-exitval => 0, -verbose => 2) if $man;
print ("autodial.pl $VERSION\n") and exit if $version;

die('--phone option is required') unless $phone_number;
die('--duration option is required') unless $session_duration;
die('--slevel option is required') unless $spk_level;
die('--mlevel option is required') unless $mic_level;
die('--input option is required') unless $input_num;
die('--dialscript option is required') unless $dialscript;

my $log = Log::Dispatch->new();

if($logfile) {
    $log->add (
        Log::Dispatch::File->new (
             name      => 'file',
             min_level => 'info',
             filename  => $logfile,
             newline   => 1,
             mode      => '>>',
             callbacks => sub {
                 my %p = @_;
                 return '['.strftime('%F %T', localtime())."][$p{level}]:$p{message}";
             }
        )
    );
}
else
{
    $log->add (
        Log::Dispatch::Screen->new (
            name      => 'screen',
            min_level => 'info',
            newline   => 1
       )
    );
}
Log::Any::Adapter->set ({category => 'autodial'}, 'Dispatch', dispatcher=>$log);

my $should_stop = 0;
my $try_interval = 1;
my $event_mask = 3 << (2 * $input_num);
my $online_mask = 0x08;
 
$SIG{TERM} = sub {
	$log->info ("Got TERM signal");
	$should_stop = 1;
};

do_work ($server_port, $server_host);

sub resolve_server_address
{
	my ($server_host) = @_;
	my ($serverip_s);
	
	while (!$should_stop) {
		$log->info ("Trying to resolve server $server_host address ... ");
		last if $serverip_s = inet_aton ($server_host);
		$log->error ("Address resolving failed");
		sleep ($try_interval);
	}
	if ($serverip_s) {
		$log->info ("Host $server_host resolved to " . inet_ntoa($serverip_s));
	}
	if ($should_stop) {
		$log->info ("Shutting down")
	}	
	return $serverip_s;
}

sub connect_to_server
{
	my ($sock, $sockaddr) = @_;
	my ($connected) = 0; 
	while (!$should_stop) {
		$log->info ("Trying to connect to server ... ");
		last if $connected = connect ($sock, $sockaddr);
		$log->error ("Server connection error");
 		sleep ($try_interval);
	}
	if ($connected) {
		$log->info ("Connected to server");
	}
	if ($should_stop) {
		$log->info ("Shutting down");
	}
}

sub do_work
{
	my ($server_port, $server_host) = @_;
	my ($sock, $serverip_s, $socket_created);
	
	$log->info ("autodial.pl version $VERSION start working");
	
	while (!$should_stop) {
		last if !($socket_created = socket ($sock, AF_INET, SOCK_STREAM, 0));
		if (!$should_stop) {
			$serverip_s = resolve_server_address ($server_host);
		}
		if (!$should_stop) {
			connect_to_server ($sock, pack_sockaddr_in($server_port, $serverip_s));
		}
		if (!$should_stop) {		
			process_msgs ($sock);
		}
		close ($sock);
		sleep ($try_interval);
	}
	if (!$socket_created) {
		$log->fatal ("Unable to create socket");
	}
	
}

sub process_msgs
{
	my ($sock) = @_;
	$log->info ("Start processing messages ...");
	while(<$sock>) {
		chomp;
	#<C,N,(12584345,R,1441558070,1441561205,1441558070,136,23,43775,0,43775,0,0,0,57.925260,60.112443,108,40,0.0010,0.0000,0.0000,0.0000,36777)>
		if(/^<C,N,\(([\d]+),([RS]),([\d]+),([\d]+),([\d]+),([\d]+),([\d]+),([\d]+),([\d]+),([\d]+)/) {
			$log->debug ("Parsing message " . $_);
			$log->debug ("Found terminal_id=$1,sending_dir=$2,state_date_time=$3,status_flags=$6,changed_inputs_bitmap=$9");
			unless ($6 & $online_mask) {
				if ($9 & $event_mask) {
					my $cmd = "$dialscript --host=$server_host --port=$server_port --terminal=$1 --phone=\"$phone_number\" --duration=$session_duration --slevel=$spk_level --mlevel=$mic_level";
					$log->info("Button press event found. Trying to execute command $cmd");
					my @result = `$cmd 2>&1`;
					$log->info("Command result is @result")
				}
			}
		}
	}
	if ($should_stop) {
		$log->info ("Shutting down");
	} else {
		$log->error ("Server connection terminated");
	}
}

__END__

=head1 NAME

autodial.pl - Decode messages and autosends start voice session command for Lineguard trackers

=head1 SYNOPSYS

autodial.pl [--host=HOST_NAME] [--port=PORT] [--logfile=LOG_FILE] --dialscript=DIAL_SCRIPT --phone=PHONE_NUMBER --duration=DURATION --slevel=SPEAKER_LEVEL --mlevel=MIC_LEVEL --input=INPUT_NUMBER

autodial.pl --help

autodial.pl --man

autodial.pl --version

=head1 OPTIONS

=over 8

=item B<--help>

Prints a brief help message and exits.

=item B<--man>

Prints the manual page and exits.

=item B<--host=HOST_NAME>

Sets optional hostname or IP address of Lineguard gateway server, default value is 127.0.0.1

=item B<--port=PORT>

Sets optional TCP port of Lineguard gateway software, default value is 7777

=item B<--logfile=LOG_FILE>

Sets optional log filename

=item B<--dialscript=DIAL_SCRIPT>

Sets the program to execute to start voice session

=item B<--phone=PHONE_NUMBER>

Sets the phone number for start voice session command

=item B<--duration=DURATION>

Sets maximum duration in seconds for start voice session command

=item B<--slevel=SPEAKER_LEVEL>

Sets speaker level from 0 to 255 for start voice session command

=item B<--mlevel=MIC_LEVEL>

Sets microphone level from 0 to 255 for start voice session command

=item B<--input=INPUT_NUMBER>

Sets tracker input number from 1 to 4 for "Start voice session" button

=back

=head1 DESCRIPTION

B<autodial.pl> connects to Lineguard gateway software and decodes tracker messages. The program will send start voice session command with defined parameters if it gets button press event from tracker. 

=head1 AUTHOR

Andrey Voronin. Please send all bug reports to <avoronin@gmail.com>.

=head1 COPYRIGHT

Copyright (c) 2015 Andrey Voronin<avoronin@gmail.com>

=cut
