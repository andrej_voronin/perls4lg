#!/usr/bin/env perl
use strict;
use warnings;
use Socket;
use Getopt::Long;
use Pod::Usage;

my $VERSION='1.0';

my($terminal_id, $server_port, $server_host, $phone_number, $session_duration, $spk_level, $mic_level, $man, $help, $version);

$server_port = 7777;
$server_host = 'localhost';

GetOptions('terminal=i'   => \$terminal_id,
           'port=i'     => \$server_port,
           'host=s'     => \$server_host,
           'phone=s'    => \$phone_number,
           'duration=i' => \$session_duration,
           'slevel=i'   => \$spk_level,
           'mlevel=i'   => \$mic_level,
           'help'       => \$help,
           'man'        => \$man,
           'version'    => \$version) or die('Error in command line arguments');

pod2usage (1) if $help;
pod2usage (-exitval => 0, -verbose => 2) if $man;
print ("dial.pl $VERSION\n") and exit if $version; 
	
die ('--terminal option is required') unless $terminal_id;
die ('--phone option is required') unless $phone_number;
die ('--duration option is required') unless $session_duration;
die ('--slevel option is required') unless $spk_level;
die ('--mlevel option is required') unless $mic_level;

do_work ($server_host, $server_port);

sub resolve_server_address
{
	my ($server_host) = @_;
	my ($serverip_s);
	
	print ("Trying to resolve server $server_host address ... \n");
	die ("Address resolving failed") unless ($serverip_s = inet_aton($server_host));
	print ("Host $server_host resolved to ".inet_ntoa($serverip_s)."\n");
	return $serverip_s;
}

sub connect_to_server
{
	my ($sock, $sockaddr) = @_;
	print ("Trying to connect to server ... \n");
	die ("Server connection error") unless (connect ($sock, $sockaddr));
	print ("Connected to server\n");
}

sub do_work
{
	my ($server_host, $server_port) = @_;
	my ($sock, $serverip_s, $socket_created);
	
	print ("dial.pl version $VERSION start working\n");
	
	die ("Unable to create socket") unless ($socket_created = socket($sock, AF_INET, SOCK_STREAM, 0));
	$serverip_s = resolve_server_address ($server_host);
	connect_to_server ($sock, pack_sockaddr_in ($server_port, $serverip_s));
	send_command ($sock);
	close ($sock);
}

sub send_command
{
	my ($sock) = @_;
	my $cmd = "voice$terminal_id,$phone_number,$session_duration,$spk_level,$mic_level,0,0\r\n";
	die ('Command sending error') unless send($sock, $cmd, 0);
	print ("Command sended\n");
}

__END__

=head1 NAME

dial.pl - Sends start voice session command for Lineguard trackers

=head1 SYNOPSYS

dial.pl [--host=HOSTNAME] [--port=PORT] --terminal=TERMINAL_ID --phone="PHONE_NUMBER" --duration=DURATION --slevel=SPEAKER_LEVEL --mlevel=MIC_LEVEL

dial.pl --help

dial.pl --man

dial.pl --version

=head1 OPTIONS

=over 8

=item B<--help>

Prints a brief help message and exits.

=item B<--man>

Prints the manual page and exits.

=item B<--host=HOSTNAME>

Sets optional hostname or IP address of Lineguard gateway server, default value is 127.0.0.1

=item B<--port=PORT>

Sets optional TCP port of Lineguard gateway software, default value is 7777

=item <B--terminal=TERMINAL_ID>

Sets terminal identificator for start voice session command

=item B<--phone="PHONE_NUMBER">

Sets the phone number for start voice session command

=item B<--duration=DURATION>

Sets maximum duration in seconds for start voice session command

=item B<--slevel=SPEAKER_LEVEL>

Sets speaker level from 0 to 255 for start voice session command

=item B<--mlevel=MIC_LEVEL>

Sets microphone level from 0 to 255 for start voice session command

=back

=head1 DESCRIPTION

B<dial.pl> connects to Lineguard gateway software and sends start voice session command with defined parameters 

=head1 AUTHOR

Andrey Voronin. Please send all bug reports to <avoronin@gmail.com>.

=head1 COPYRIGHT

Copyright (c) 2015 Andrey Voronin<avoronin@gmail.com>

=cut
